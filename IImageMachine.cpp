//////////////////////////////////////////////////////////////////////

#include "IImageMachine.h"
#include "ImageMachine.h"

//////////////////////////////////////////////////////////////////////
// IGPUParticleEngine interface
//////////////////////////////////////////////////////////////////////
IImageMachine* GetImageMachine()
{
	IImageMachine* imageObj;

	imageObj = (IImageMachine*) (new ImageMachine());
	return imageObj;
}

HRESULT ReleaseImageMachine(IImageMachine* imageObj)
{
	if (imageObj)
	{
		delete (ImageMachine *)imageObj;
		imageObj = NULL;
	}
	return S_OK;
}


